# Everything is Normal #

### Summary ###
Everything is Normal is a 2D side-scroller created in Unity using C#. It was designed and created for the 2014 Global Game Jam in a four person team, with three programmers and one artist. 

### Information Page ###
http://globalgamejam.org/2014/games/everything-normal

##3 How To Run ###
Please run the executable to run the project. This project is now outdated from the current Unity and can't be run through Unity.